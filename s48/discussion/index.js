let posts = [];
let count = 1;

// _______Add post data_______

document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	e.preventDefault();

	posts.push({
		id : count,
		title : document.querySelector('#txt-title').value,
		body : document.querySelector('#txt-body').value
	});

	count++;

	showPosts(posts);
	alert('Successfully Added');
});

// _______Show posts_______

const showPosts = (posts) => {
	let postEntries = '';

	// We are using the forEach method to iterate every post in our website. So that the website will show our created posts one by one.
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// _______Edit post_______

const editPost = (id) => {
	// use template literals (``) to pass through the id
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
}

// _______Update post_______
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0; i< posts.length; i++){

		// ginamit natin ito (toString) para mahanap ang kailangan iupdate
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated.')

			break;
		}
	}
});


// *************** Activity s48 ***************

// _______Delete post_______
/*
const deletePost = (id) => {
	posts = posts.filter((post) => post.id !== id);

	const postElement = document.querySelector(`#post-${id}`);

	if (postElement) {
		postElement.remove();
	}
	alert('Post deleted.');
}
*/

/*
In the updated code, I added the deletePost() function. It uses the filter() method to remove the post with the specified id from the posts array. It also selects the corresponding post element from the DOM using the id and removes it using the remove() method. Finally, it displays an alert message to indicate that the post has been deleted.
*/

// ************ Sir Vasco's version ************

// _______Delete post_______

const deletePost = (id) => {
    posts = posts.filter((post) => {
        if (post.id.toString() !== id) {
            return post;
        }
    });

    document.querySelector(`#post-${id}`).remove();
}


// ************************************************