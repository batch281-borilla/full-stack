let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

    return collection;

}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    
  collection[collection.length] = element;
   return collection;
}

function dequeue() {
    // In here you are going to remove the first element in the array

    if (collection.length === 0) {
        return "The queue is empty.";
      }

      var dequeuedElement = collection[0];
      for (var i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1];
      }
      collection.length--;
      
      return collection;
}

function front() {
    // you will get the first element

   if (collection.length === 0) {
       return "The queue is empty.";
     }

     return collection[0];
}


function size() {
     // Number of elements 

  var count = 0;
    for (var element in collection) {
      if (collection.hasOwnProperty(element)) {
        count++;
      }
    }
    return count;
}

function isEmpty() {
    //it will check whether the function is empty or not

    return collection === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};