const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
})

const updateName = () => {
	spanFullName.innerHTML = txtFirstName.value + '' + txtLastName.value;
}

txtLastName.addEventListener('keyup', updateName)

// ************ Sir Vasco's version ************
/*
const updateFullName = () => {
    let firstName = txtFirstName.value;
    let lastName = txtLastName.value;

    spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName);
*/